(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate nil)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website publishing.
(setq org-publish-project-alist
      (list
       (list "environment"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["environment.org"]
             :preparation-function '(disable-babel)
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "article-ipdps"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["article-ipdps.org"]
             :preparation-function '(enable-babel)
             :publishing-function '(org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public")
       (list "research-report"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["research-report.org"]
             :preparation-function '(enable-babel)
             :publishing-function '(org-html-publish-to-html
                                    org-latex-publish-to-pdf-verbose)
             :publishing-directory "./public")
       (list "results"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["2stagecs.org"]
             :preparation-function '(enable-babel)
             :completion-function '(fix-svg)
             :publishing-directory "."
             :publishing-function '(org-babel-execute-file))
       (list "figures"
             :base-directory "./figures"
             :base-extension "png\\|jpg\\|gif\\|svg\\|ico"
             :recursive t
             :publishing-directory "./public/figures"
             :publishing-function '(org-publish-attachment))
       (list "2stagecs"
             :components '("environment" "article-ipdps" "research-report"))
       (list "2stagecs-static"
             :components '("figures"))))

(provide 'publish)
