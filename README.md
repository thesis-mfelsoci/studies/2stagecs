# Direct solution of larger coupled sparse/dense linear systems using low-rank compression on single-node multi-core machines in an industrial context

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/studies/2stagecs/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/studies/2stagecs/-/commits/master)

While hierarchically low-rank compression methods are now commonly available in
both dense and sparse direct solvers, their usage for the direct solution of
coupled sparse/dense linear systems has been little investigated. The solution
of such systems is though central for the simulation of many important physics
problems such as the simulation of the propagation of acoustic waves around
aircrafts. Indeed, the heterogeneity of the jet flow created by reactors often
requires a Finite Element Method (FEM) discretization, leading to a sparse
linear system, while it may be reasonable to assume as homogeneous the rest of
the space and hence model it with a Boundary Element Method (BEM)
discretization, leading to a dense system. In an industrial context, these
simulations are often operated on modern multicore workstations with
fully-featured linear solvers. Exploiting their low-rank compression techniques
is thus very appealing for solving larger coupled sparse/dense systems (hence
ensuring a finer solution) on a given multicore workstation, and – of course –
possibly do it fast. The standard method performing an efficient coupling of
sparse and dense direct solvers is to rely on the Schur complement functionality
of the sparse direct solver. However, to the best of our knowledge, modern
fully-featured sparse direct solvers offering this functionality return the
Schur complement as a non compressed matrix. In this paper, we study the
opportunity to process larger systems in spite of this constraint. For that we
propose two classes of algorithms, namely multi-solve and multi-factorization,
consisting in composing existing parallel sparse and dense methods on well
chosen submatrices. An experimental study conducted on a 24 cores machine
equipped with 128 GiB of RAM shows that these algorithms, implemented on top of
state-of-the-art sparse and dense direct solvers, together with proper low-rank
assembly schemes, can respectively process systems of 9 million and 2.5 million
total unknowns instead of 1.3 million unknowns with a standard coupling of
compressed sparse and dense solvers.

[**Explore**](https://thesis-mfelsoci.gitlabpages.inria.fr/studies/2stagecs)
