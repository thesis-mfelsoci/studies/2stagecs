The format of an ~.fstab~ description file is very straightforward. Each line
must begin with either a =D=/=d= or a =F=/=f= indicating whether a directory or
a file should be initialized. In case of a directory, the letter is followed by
a colon and the destination path of the directory. In case of a file, follows a
colon, the source path of the file, a colon and the destination path in the
target filesystem.

To describe the filesystem from Section [[#gcvb]], we use the ~benchmarks.fstab~
description file:

#+HEADER: :tangle benchmarks.fstab :padline no
#+BEGIN_SRC text
D:data/all/input
D:data/all/references
D:data/all/templates
D:results
D:slurm
F:benchmarks/sbatch:data/all/templates/sbatch/sbatch
F:sources/files/wrapper-in-core.sh:data/all/templates/wrapper-in-core/wrapper.sh
F:sources/files/submit.sh:scripts/submit.sh
F:sources/files/rss.py:scripts/rss.py
F:sources/files/inject.py:scripts/inject.py
F:sources/files/parse.sh:scripts/parse.sh
F:sources/files/wrapper-python-task.sh:scripts/wrapper-python-task.sh
F:benchmarks/config.yaml:config.yaml
F:benchmarks/benchmarks.yaml:benchmarks.yaml
F:channels.scm:channels.scm
F:experiments.scm:experiments.scm
#+END_SRC
